package ru.pakc.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.pakc.model.Client;
import ru.pakc.repository.ClientRepo;

import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientService {
    private final ClientRepo clientRepo;

    public boolean createClient(Client client) {
        String phoneNumber = client.getPhoneNumber();
        if (clientRepo.findByPhoneNumber(phoneNumber) != null) return false;
        log.info("Создан новый пользовател с телефоном: {}", phoneNumber);
        client.setAmount("0");
        client.setCartNumber("0000 0000 0000 0001");                 // реализовать создание номера карты
        client.setPassword(generatePin());
        clientRepo.save(client);
        return true;
    }

    public List findAll() {
        return clientRepo.findAll();
    }

    private String generatePin() {
      Random random = new Random();
      int randomPin = random.nextInt(9999) + 1;
      return Integer.toString(randomPin);
    }

    public Client findById(Long id) {
        return clientRepo.findById(id).orElse(null);
    }
}
