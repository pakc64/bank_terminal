package ru.pakc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankPakcApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankPakcApplication.class, args);
    }
}
