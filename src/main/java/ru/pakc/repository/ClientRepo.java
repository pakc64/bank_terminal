package ru.pakc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pakc.model.Client;

@Repository
public interface ClientRepo extends JpaRepository<Client, Long> {


    Client findByPhoneNumber(String phoneNumber);
}
