package ru.pakc.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pakc.model.Client;
import ru.pakc.service.ClientService;

@Controller
@RequiredArgsConstructor
public class MainController {
    private final ClientService clientService;

    @GetMapping("/")
    public String authorizationPage(Model model) {
        model.addAttribute("client", clientService.findAll());
        return "authorization";
    }

    @PostMapping("/authorization")
    public String logIn() {
     return "redirect:/personal-account";
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String createClient(Client client, Model model) {
        if (!clientService.createClient(client)) {
            model.addAttribute("errorMessage", "Пользователь с таким номером телефона уже существует:  "
                    + client.getPhoneNumber());
            return "registration";
        }
        clientService.createClient(client);
        return "redirect:/client-info/" + client.getId();

    }

    @GetMapping("/client-info/{id}")
    public String clientInformation(@PathVariable Long id, Model model) {
        model.addAttribute("client", clientService.findById(id));
        return "client-info";
    }

    @GetMapping("/account/{id}")
    public String personalAccount(@PathVariable Long id, Model model) {
        return "personal-account";
    }
}
